# frozen_string_literal: true

Rails.application.routes.draw do
  root 'tweets#index'
  get 'top/login'
  post 'top/login'
  get 'top/logout'
  resources :users, only: %i[index new create destroy]
  resources :tweets, only: %i[index new create destroy]
  resources :likes, only: %i[create destroy]
end
